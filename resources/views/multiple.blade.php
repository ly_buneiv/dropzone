<div class="card card-default mydropzone dev_{{ $class_name }}">
    {{ Form::hidden('count_item', $count ?? 0, ['class' => 'count_item']) }}
    {{ Form::hidden('input_name', $input_name, ['class' => 'input_name']) }}
    {{ Form::hidden('has_caption', $has_caption, ['class' => 'has_caption']) }}
    {{ Form::hidden('has_description', $has_description, ['class' => 'has_description']) }}
    <div class="card-header">
        <button class="photo-info btn btn-default form-control {{ config('dropzone.multiple_button_class') }}" type="button" onclick="triggerAddImageMultiple('{{ $class_name }}')">{{ __('Add Photo') }}</button>
    </div>
    <div class="card-body multiple_dropzone {{ config('dropzone.multiple_dropzone_class') }}">
        <div class="{{ $class_name }}">
            @php
                if(old($input_name)){
                    $items = old($input_name);
                    // dd($items);
                }
            @endphp
            @if(isset($items) && $count > 0)
                @foreach($items as $key => $val)
                    @php
                        $main_name_input = $input_name.'['.$key.']';
                        $array_image_url = explode('/', $val['image_url'] ?? '');
                        $val['image_name'] = end($array_image_url);
                    @endphp
                    <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                        <div class="dz-image">
                            <img data-dz-thumbnail="" src="{{ $val['image_url'] ?? '' }}" />
                        </div>
                        <div class="dz-details">
                            <div class="dz-filename">
                                <span data-dz-name>
                                    <label class="no_click">
                                        @if(array_key_exists('main', $val) && $val['main'])
                                            {{ Form::radio($main_name_input.'[main]', 1, true, ['onchange' => 'changeMain(this)', 'class' => 'radio_main']) }}
                                        @else
                                            {{ Form::radio($main_name_input.'[main]', 0, false, ['onchange' => 'changeMain(this)', 'class' => 'radio_main']) }}
                                        @endif
                                        {{ __('Main') }}
                                    </label>
                                </span>
                            </div>
                            <div class="dz-size" data-dz-size></div>
                        </div>
                        <div class="dz-progress">
                            <span class="dz-upload" data-dz-uploadprogress></span>
                        </div>
                        <div class="dz-success-mark"><span></span></div>
                        <div class="dz-error-mark"><span></span></div>
                        <div class="dz-error-message"><span data-dz-errormessage></span></div>
                        <div class="hidden_file" data-value="{{ $val['image_name'] ?? '' }}">
                            {{ Form::hidden($main_name_input.'[image_url]', $val['image_url']) }}
                            {{ Form::hidden($main_name_input.'[image_name]', $val['image_name']) }}
                        </div>
                        <a data-name="{{ $val['image_name'] ?? '' }}" class="dz-remove" data-dz-remove="" onclick="removeImageDropzone(this)">{{ __('Remove file') }}</a>

                        @if($has_caption)
                            <br/>
                            <div class="input-field">
                                <input class="form-control caption" placeholder="caption" type="text" value="{{ $val['caption'] ?? '' }}" name="{{ $main_name_input.'[caption]' }}">
                            </div>
                        @endif
                        @if($has_description)
                            <br/>
                            <div class="input-field">
                                <textarea rows="5" placeholder="description" class="form-control description" name="{{ $main_name_input.'[description]' }}">{{ $val['description'] ?? '' }}</textarea>
                            </div>
                        @endif
                    </div>
                @endforeach

            @else
                <div class="dz-message" data-dz-message><span>Drop files here to upload</span></div>
            @endif
            <div class="fallback">
                <input name="file" type="file"/>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="clearfix click_multiple">&nbsp;</div>
    </div>
</div>
{{ Form::hidden('base_url', url('/'), ['class' => 'base_url']) }}
