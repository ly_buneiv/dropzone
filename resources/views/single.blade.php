@php
    if(old($input_name))
        $image_name = old($input_name);
    if(old($input_name.'_url'))
        $image_url = url(old($input_name.'_url'));
@endphp
<div class="card card-default mydropzone dev_{{ $class_name }}">
    {{ Form::hidden($input_name, $image_name, ['class' => 'photo']) }}
    {{ Form::hidden($input_name.'_url', $image_url, ['class' => 'photo_url']) }}
    {{ Form::hidden('has_caption', $has_caption, ['class' => 'has_caption']) }}
    {{ Form::hidden('has_description', $has_description, ['class' => 'has_description']) }}
    <div class="card-header">
        <button class="photo-info btn btn-default form-control {{ config('dropzone.single_button_class') }}" type="button" onclick="triggerAddImageSingle('{{ $class_name }}')">{{ __('Add Photo') }}</button>
    </div>
    <div class="card-body single_dropzone {{ config('dropzone.single_dropzone_class') }}">
        <div class="dropzone_single {{ $class_name }}">
            <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete exist_image">
                <div class="dz-image">
                    @if($image_url)
                        <img data-dz-thumbnail="" src="{{ $image_url }}" />
                    @else
                        <img data-dz-thumbnail="" src="{{ asset('vendor/lybuneiv/dropzone/img/default.jpg') }}" style="width: 100%;" />
                    @endif
                </div>
                <div class="dz-details">
                    <div class="dz-filename"><span data-dz-name>
                        @if(!$image_url)
                            {{ "default.png" }}
                        @endif
                    </span></div>
                    <div class="dz-size" data-dz-size></div>
                </div>
                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                <div class="dz-success-mark"><span></span></div>
                <div class="dz-error-mark"><span></span></div>
                <div class="dz-error-message"><span data-dz-errormessage></span></div>

                @if($image_name)
                    <a data-name="{{ $image_name }}" class="dz-remove" data-dz-remove="" onClick="removeImageDropzone(this)">{{ __('Remove file') }}</a>
                @endif

            </div>
            <div class="fallback">
                <input name="file" type="file"/>
            </div>
        </div>
        <div class="" style="padding:0 10px;">
            @if($has_caption)
                <br/>
                <div class="input-field">
                    <input class="form-control caption" placeholder="caption" type="text" value="{{ $text_caption ?? '' }}" name="{{ $input_name.'_caption' }}">
                </div>

            @endif
            @if($has_description)
                <br/>
                <div class="input-field">
                    <textarea rows="5" placeholder="description" class="form-control description" name="{{ $input_name.'_description' }}">{{ $text_description ?? '' }}</textarea>
                </div>
            @endif
        </div>


    </div>
</div>
{{ Form::hidden('base_url', url('/'), ['class' => 'base_url']) }}
