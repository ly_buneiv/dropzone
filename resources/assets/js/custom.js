var base_url = $('.base_url').val();

function addCaptionImage(obj = '', classname= 'hidden_caption1')
{
    var el = $(obj);
    var value = el.val();
    $('.'+classname).val(value);
}


function triggerAddImageSingle(class_name = '')
{
    var el = $('.'+class_name);
    console.log(el);
    el.closest('.dz-clickable').trigger('click');
    // el.closest('.single_dropzone').trigger('click');
}

function triggerAddImageMultiple(class_name = '')
{
    var el = $('.'+class_name);
    console.log(el);
    el.closest('.dz-clickable').trigger('click');
}

$(function(){
    $('.multiple_dropzone > .not_clickable_dropzone').on('click', function(e){
        e.stopPropagation();
        e.preventDefault();
    });

    // $('.dz-image').on('click', function(){
    //     $(this).closest('.dz-clickable').trigger('click');
    // });

    $('.click_multiple').on('click', function(){
        $(this).closest('.multiple_dropzone').find('.dz-clickable').trigger('click');
    })

    $(document).on("click", ".dz-preview" , function(e) {
        var $target = $(e.target);

        if (!$target.is('input') && !$target.is('textarea') && !$target.is('.no_click')) {
            $(this).closest('.dz-clickable').trigger('click');
        }

    });
});

function changeMain(obj)
{
    var el = $(obj);
    el.closest('.multiple_dropzone').find('.radio_main').prop('checked', false);
    el.prop('checked', true);
}

function generateImageCaption(class_name, has_caption, has_description)
{
    var html = '';
    if(has_caption){
        html += '<br/><div class="input-field">';
        html += '<input type="text" value="" name="'+class_name+'_caption" placeholder="caption" class="'+class_name+'_caption form-control caption">';
        html += '</div>';
    }

    if(has_description){
        html += '<br/><div class="input-field">';
        html += '<textarea rows="5" value="" name="'+class_name+'_description" placeholder="description" class="'+class_name+'_description form-control description"></textarea>';
        html += '</div>';
    }
    return html;
}

function generateMultipleImageCaption(class_name, has_caption, has_description, count)
{
    var html = '';
    if(has_caption){
        html += '<br/><div class="input-field">';
        html += '<input type="text" value="" name="'+class_name+'['+count+'][caption]" placeholder="caption" class="'+class_name+'_caption form-control caption">';
        html += '</div>';
    }

    if(has_description){
        html += '<br/><div class="input-field">';
        html += '<textarea rows="5" value="" name="'+class_name+'['+count+'][description]" placeholder="description" class="'+class_name+'_description form-control description"></textarea>';
        html += '</div>';
    }
    return html;
}

function generateDefautlImage(class_name)
{
    var html = '';

    html += '<div class="dz-preview dz-processing dz-image-preview dz-success dz-complete exist_image">';
    html += '<div class="dz-image">';
    // img = $('<img data-dz-thumbnail="" src="' + base_url + '/vendor/lybuneiv/dropzone/img/default.jpg" style="width: 100%;">').click(triggerAddImageSingle())
    html += '<img data-dz-thumbnail="" src="' + base_url + '/vendor/lybuneiv/dropzone/img/default.jpg" style="width: 100%;">';
    html += '</div>';
    html += '<div class="dz-details">';
    html += '<div class="dz-filename"><span data-dz-name="">default.png</span></div>';
    html += '</span></div>';
    html += '<div class="dz-size" data-dz-size=""></div>';
    html += '</div>';
    html += '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>';
    html += '<div class="dz-success-mark"><span></span></div>';
    html += '<div class="dz-error-mark"><span></span></div>';
    html += '<div class="dz-error-message"><span data-dz-errormessage=""></span></div>';

    html += '</div>';

    return html;
}

function generateOption(el = '', class_name = '')
{
    var text = '';   
    var dropzone_url = base_url + '/admin/image/upload-single/'+class_name;
    var has_caption = el.closest('.mydropzone').find('.has_caption').val();
    var has_description = el.closest('.mydropzone').find('.has_description').val();
    var option = {
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        addRemoveLinks: true,
        autoProcessQueue :true,
        dictDefaultMessage: text,
        acceptedFiles: 'image/png,.jpg,.jpeg',
        clickable: true,
        // clickable: '.dev_'+class_name+' .single_dropzone',
        url: dropzone_url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function() {
            var myDropzone = this;
            this.on("success", function(files, response) {
                el.closest('.mydropzone').find('.photo').val(response.name);
                el.closest('.mydropzone').find('.photo_url').val(response.image_url);
                el.find('.dz-size').addClass('display_none');
                // caption = generateImageCaption(class_name, has_caption, has_description);
                // el.find('.dz-preview').append(caption)

            });
            this.on("error", function(files, response) {
              console.log(response);
            });
            this.on("removedfile", function(file){
                var name = file.name;
                var rootElement = $('.hidden_file[data-value="'+name+'"]');
                var _token = $('input[name="_token"]').val();
                var filename = rootElement.find('.hidden_value').val();
                el.closest('.mydropzone').find('.photo').val('');
                el.closest('.mydropzone').find('.photo_url').val('');

                var html = generateDefautlImage(class_name);
                if(this.files.length == 0){
                    el.html(html);
                    // caption = generateImageCaption(class_name, has_caption, has_description);

                    // el.find('.dz-preview').append(caption)

                }
             });
            this.on('addedfile', function(file) {
                el.find('.exist_image').remove();
                if (this.files.length > 1) {
                    console.log('ss');
                  this.removeFile(this.files[0]);
                }
            });

            this.on("maxfilesexceeded", function(file){
                // if (this.files.length > 1) {
                //   this.removeFile(this.files[0]);
                // }
                alert("No more files please!");
            });
        }
    };

    return option;
}

function generateOptionMutiple(el = '', class_name = '')
{
    var text = 'Drop files here to upload';
    var count = $('.dev_'+class_name).find('.count_item').val();
    // console.log(count);
    count = parseInt(count);
    if(count > 0)
        text = '';

    var has_caption = el.closest('.mydropzone').find('.has_caption').val();
    var has_description = el.closest('.mydropzone').find('.has_description').val();

    var input_name = el.closest('.mydropzone').find('.input_name').val();
    var dropzone_url = base_url + '/admin/image/upload-multiple/'+class_name 
    var option = {
        uploadMultiple: true,
        parallelUploads: 1,
        maxFiles: 30,
        addRemoveLinks: true,
        autoProcessQueue :true,
        dictDefaultMessage: text,
        acceptedFiles: 'image/png,.jpg,.jpeg',
        // clickable: '.dev_'+class_name+' .multiple_dropzone > :not(.not_clickable_dropzone)',
        url: dropzone_url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        init: function() {
            var myDropzone = this;


            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function() {
              // Gets triggered when the form is actually being sent.
              // Hide the success button or the complete form.
            });
            this.on("successmultiple", function(files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.

                var html = '';
                var caption = '';
                $.each( files, function( key, val ) {
                    count = count + 1;
                    var value = response.filename[key];
                    caption = '';
                    html += '<div class="hidden_file" data-value="'+value['fullname']+'">';
                    html += '<input type="hidden" value="'+value['name']+'" name="'+input_name+'['+count+'][image_name]" class="hidden_value">';
                    html += '<input type="hidden" value="'+value['image_url']+'" name="'+input_name+'['+count+'][image_url]" class="hidden_value">';
                    html += '</div>';

                    html += generateMultipleImageCaption(class_name, has_caption, has_description, count);

                    var rootImage = $(val.previewElement);
                    var radio = '<label class="no_click"><input class="not_clickable_dropzone radio_main" type="radio" value="'+value['name']+'" name="'+input_name+'['+count+'][main]" onChange="changeMain(this);"> Main</label>';
                    rootImage.find('.dz-details').find('.dz-filename').find('span').html(radio);
                    rootImage.append(html);


                });
                $('.dev_'+class_name).find('.count_item').val(count);
                // $('.append_file_name').append(html);
                $('.dz-size').addClass('display_none');
                $('.dz-message').remove();

            });
            this.on("errormultiple", function(files, response) {

              console.log(response);
            });
            this.on("removedfile", function(file){

                var name = file.name;

                var rootElement = $('.hidden_file[data-value="'+name+'"]');
                var _token = $('input[name="_token"]').val();
                var filename = rootElement.find('.hidden_value').val('');
                rootElement.remove();

             });

        }
    };

    return option;
}


