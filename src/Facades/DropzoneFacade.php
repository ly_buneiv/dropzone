<?php

namespace Lybuneiv\Dropzone\Facades;

use Illuminate\Support\Facades\Facade;

class DropzoneFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'dropzone';
    }
}
