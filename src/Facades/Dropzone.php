<?php

namespace Lybuneiv\Dropzone\Facades;

class Dropzone
{

    /**
     * Render dropzone css
     * @return view as string
     */
    public function renderDropzoneCss()
    {
        return view('Dropzone::dropzone_css');
    }

    /**
     * Render dropzone js
     * @return view as string
     */
    public function renderDropzoneJs()
    {
        return view('Dropzone::dropzone_js');
    }

   /**
     * Render dropzone html
     * @return view as string
     */
    public function renderSingle($image_url, $input_name = 'photo', $class = 'dropzone_single', $has_caption = false, $text_caption = '', $has_description = false, $text_description = '')
    {

        $data['class_name'] = $class;
        $data['image_url'] = $image_url;
        $array_image_url = explode("/", $image_url);
        $data['image_name'] = end($array_image_url);
        $data['input_name'] = $input_name;
        $data['has_caption'] = $has_caption;
        $data['text_caption'] = $text_caption;
        $data['has_description'] = $has_description;
        $data['text_description'] = $text_description;
        return view('Dropzone::single', $data);
    }

    /**
     * Render dropzone js
     * @return view as string
     */
    public function renderSingleJs($class = '')
    {
        $data['class_name'] = $class;
        return view('Dropzone::single_js', $data);
    }

    /**
     * Render dropzone html
     * @return view as string
     */
    public function renderMultiple($images = [], $input_name = 'photo', $class = 'dropzone_single', $has_caption = false, $has_description = false)
    {
        $data['class_name'] = $class;
        $data['items'] = $images;
        $data['input_name'] = $input_name;
        $data['count'] = count($images);
        // dd($data);
        $data['has_caption'] = $has_caption;
        $data['has_description'] = $has_description;

        return view('Dropzone::multiple', $data);
    }

    /**
     * Render dropzone js
     * @return view as string
     */
    public function renderMultipleJs($class = '')
    {
        $data['class_name'] = $class;
        return view('Dropzone::multiple_js', $data);
    }
}
