<?php
namespace Lybuneiv\Dropzone;

use Illuminate\Support\Facades\Storage;

class Upload
{

	public function uploadToPath($file)
    {
        $fileName = md5($file->getFilename());
        $extension = '.' . $file->getClientOriginalExtension();
        $fileName .= $extension;

        $path = storage_path('tmp/uploads');

        if (! file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if ($file->move($path, $fileName)) {
            return $fileName;
        }
        return false;
    }

	public function uploadToTmp($file)
    {
        $fileName = md5($file->getFilename());
        $extension = '.' . $file->getClientOriginalExtension();
        $fileName .= $extension;

        $path = storage_path('tmp/uploads');

        if (! file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if ($file->move($path, $fileName)) {
            return $fileName;
        }
        return false;
    }

	public function uploadProfile($file)
	{
		if ($file) {
			$fileName = md5($file->getFilename());
			$extension = '.' . $file->getClientOriginalExtension();
			$fileName .= $extension;

			$disk = config('filesystems.default');
			$path = config('filesystems.photo');

        	$store = Storage::disk($disk)->put($path . '/' . $fileName, file_get_contents($file));

            if ($store) {
                return $fileName;
            }
		}
		return false;
	}

	public function uploadImagCv($file)
	{
		if ($file) {
			$fileName = md5($file->getFilename());
			$extension = '.' . $file->getClientOriginalExtension();
			$fileName .= $extension;

			$disk = config('filesystems.default');
			$path = config('filesystems.photo');

        	$store = Storage::disk($disk)->put($path . '/' . $fileName, file_get_contents($file));

            if ($store) {
                return $fileName;
            }
		}

		return false;
	}

	public function uploadCV($file, $name)
    {
        $fileName = md5($name).'_'.time().date('dmY');
        $extension = '.' . $file->getClientOriginalExtension();
        $fileName .= $extension;

        $path = storage_path('tmp/uploads');

        if (! file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if ($file->move($path, $fileName)) {
            return $fileName;
        }
        return false;
    }

	public function deleteFile($file_name)
	{
		$file_name = storage_path('images/guides').'/'.$file_name;
		if (file_exists($file_name)) {
			\File::delete($file_name);
		}
	}

	public function deleteFileName($file_name = '',$path = '')
	{
		$file_name = $path.'/'.$file_name;
		if (env('IMGS3') === 's3') {
			$exists = Storage::disk('s3')->exists('storage/'.$file_name);
			if ($exists) {
				Storage::disk('s3')->delete('storage/'.$file_name);
			}
		} else {
			\File::delete($file_name);
		}
    }

    public function uploadToTmp1($file){

		$fileName = md5($file->getFilename());
		$extension = '.' . $file->getClientOriginalExtension();
		$fileName .= $extension;

		$temporaryPath = sys_get_temp_dir();
		if($file->move($temporaryPath, $fileName)){
			return $fileName;
		}
		return false;
	}
}
