<?php
namespace Lybuneiv\Dropzone\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Auth;
use Lybuneiv\Dropzone\Upload;
use Response;
use File;

class ImageController extends Controller
{
    protected $activity;

    public function __construct() {
        $this->upload = new Upload();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $photoProfile = $this->upload->uploadToTmp1($file);

        if($photoProfile){
            return \Response::json(['success' => true, 'path' => $photoProfile, 'url' => url('media/large/'.$photoProfile)]);
        }

        return \Response::json([
                            "success" => false,
                            "message" => 'Upload fail'
                        ],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if(!Auth::user()->can('modify_destination'))
            return view('admin.common.no-permission');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActivityRequest $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadSingle($optional, Request $request)
    {
        $file = $request->file('file');
        // $fullname = $file->getClientOriginalName();
        // $upload_success = $this->upload->uploadToTmp1($file);
        $image_url = $file->store('public/dropzone/photos');
        $image_url = str_replace("public", "storage", $image_url);
        $array_image_url = explode('/', $image_url);
        $image_name = end($array_image_url);
        if ($image_url) 
            return \Response::json(['success' => 200, 'name' => $image_name,'fullname' => $image_name, 'image_url' => $image_url]);

        return \Response::json([
                            "success" => false,
                            "message" => 'Upload fail!'
                        ],500);
    }

    public function uploadMultiple($optional, Request $request)
    {

        $files = $request->file('file');
        $fileName = [];

        foreach ($files as $key => $file) {
            // $fullname = $file->getClientOriginalName();
            // $upload_success = $this->upload->uploadToTmp1($file);

            $image_url = $file->store('public/dropzone/photos');
            $image_url = str_replace("public", "storage", $image_url);
            $array_image_url = explode('/', $image_url);
            $image_name = end($array_image_url);

            if ($image_url) {
                $fileName[] =  [
                    'name' => $image_name,
                    'fullname' => $image_name,
                    'image_url' => $image_url
                ]; 
            }
        }
        return Response::json(['success' => 200,'filename' => $fileName]);
    }
}
