# Readme

Dropzone module.

## Installation

You can install the package using Composer:

```bash
composer require lybuneiv/dropzone
```

Then add the service provider to config/app.php:

```bash
Lybuneiv\Dropzone\DropzoneServiceProvider::class
```

Publish the all file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --force
```

 Or

 Publish the config file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --tag="config"
```
Publish the public file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --tag="public"
```

Publish the view file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Dropzone\DropzoneServiceProvider" --tag="views"
```


Then symlink storage link:

```bash
php artisan storage:link
```

## Usage

Add dropzone style in header

```bash
{!! Dropzone::renderDropzoneCss() !!}
```

Add dropzone script

```bash
{!! Dropzone::renderDropzoneJs() !!}
```


Single image html
$has_caption => true or false
$has_description => true or false

You will get input_data => input_name, input_name_url, input_name_caption, input_name_description

```bash
{!! Dropzone::renderSingle($url_image, 'input_name', 'class_name', $has_caption, 'text_caption', $has_description, 'text_description') !!}
```

Single image js

```bash
{!! Dropzone::renderSingleJs('class_name') !!}
```

Multiple image html
$has_caption => true or false
$has_description => true or false

$array_image = [
	[
        'image_url' => 'https://discoverlaos.today/img/promotion/07ac6a3aa112198b491b2bcd79a6d2a6.jpg?p=image700x450',
        'main' => true,
        'caption' => 'caption1',
        'description' => 'description1'
    ]
];

You will get input_data => input_name[0][image_name], input_name[0][image_url], input_name[0][main], input_name[0][caption], input_name[0][description]

```bash
{!! Dropzone::renderMultiple($array_image, 'input_name', 'class_name', $has_caption, $has_description) !!}
```

Multiple image js

```bash
{!! Dropzone::renderMultipleJs('class_name') !!}
```
