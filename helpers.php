<?php

function getImageSymTemp ($val = '', $size = 'large', $path = '')
{
    $temp = sys_get_temp_dir() .'/'. $val;
    $img_extension = explode('.', $val);
    $extension = 'jpg';

    if(\File::exists($temp)){
        return url('temp/'.$size.'/'.$val);
    }

    return url('img/'.$path.'/'.$val.'?p='.$size);
}