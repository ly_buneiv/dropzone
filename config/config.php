<?php

return [

    'views' => false,

    'single_button_class' => '',
    'single_dropzone_class' => '',

    'multiple_button_class' => '',
    'multiple_dropzone_class' => ''
];
